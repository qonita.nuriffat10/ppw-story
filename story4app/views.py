from django.shortcuts import render

# Create your views here.

def profile(request):
    return render (request,'story4app/profile.html')

def index(request):
    return render (request,'story4app/index.html')

def hobby(request):
    return render (request,'story4app/hobby.html')