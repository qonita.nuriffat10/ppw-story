from django.urls import path
from . import views 
app_name = 'story4app'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('profile/', views.index, name='index'),
    path('hobby/', views.hobby,name="hobby"),
]