from django.shortcuts import render,redirect, get_object_or_404
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import PostModel

# Create your views here.

# nampilin semua data
def index(request):
    posts = PostModel.objects.all()
    context = {'posts' : posts}
    return render (request, 'story5app/index.html', context)

# buat nambahin data
def create(request):
    post_form = PostForm()
    if request.method == 'POST':
        post_form = PostForm(request.POST)
        if post_form.is_valid():
            post_form.save()
            # return redirect('create')
            return redirect('story5app:index')
            # messages.success(request, ("Mata kuliah berhasil ditambahkan!"))
        else:
            messages.warning(request,("Ulangi."))
            return redirect('story5app:create') 
        
    context = {'post_form' : post_form}
    return render (request, 'story5app/create.html', context)

def detail(request, pk):
    matkul = get_object_or_404(PostModel, id=pk)
    return render(request, 'story5app/detail.html', {'post': matkul})


def deleteItem(request, pk):
    matkul = PostModel.objects.get(id=pk)
    matkul.delete()
    # messages.success(request,(f"Mata Kuliah {matkul} berhasil dihapus."))
    return redirect('story5app:index')

