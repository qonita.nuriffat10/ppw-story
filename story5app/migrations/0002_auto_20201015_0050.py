# Generated by Django 3.0.2 on 2020-10-14 17:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story5app', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='postmodel',
            name='tahun_semester',
        ),
        migrations.AddField(
            model_name='postmodel',
            name='semester',
            field=models.CharField(choices=[('Ganjil', 'Ganjil'), ('Genap', 'Genap')], default='Ganjil', max_length=6),
        ),
        migrations.AddField(
            model_name='postmodel',
            name='tahun',
            field=models.CharField(choices=[('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023')], default='2019/2020', max_length=9),
        ),
    ]
