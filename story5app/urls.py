from django.urls import path

from . import views

app_name = 'story5app'

urlpatterns = [
    path('delete/<int:pk>', views.deleteItem, name='delete'),
    path('detail/<int:pk>', views.detail, name='detail'),
    path('create/', views.create,name='create'),
    path('', views.index, name='index'),
]