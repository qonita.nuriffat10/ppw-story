from django import forms
from .models import PostModel

class PostForm(forms.ModelForm): 
    class Meta:
        model = PostModel
        fields = ('mata_kuliah', 'dosen', 'sks', 'semester','tahun', 'ruang', 'desc')
        widgets = {
            'mata_kuliah' : forms.TextInput(attrs={'class': 'form-control'}),
            'dosen' : forms.TextInput(attrs={'class': 'form-control'}),
            'sks' : forms.NumberInput(attrs={'class': 'form-control'}),
            'semester' : forms.Select(attrs={'class': 'form-control'}),
            'tahun' : forms.Select(attrs={'class': 'form-control'}),
            'ruang' : forms.TextInput(attrs={'class': 'form-control'}),
            'desc' : forms.TextInput(attrs={'class': 'form-control'}),
        }