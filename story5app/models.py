from django.db import models

# Create your models here.

class PostModel(models.Model):
    TERM_CHOICES = [
        ('Ganjil', 'Ganjil'),
        ('Genap', 'Genap')
    ]
    YEAR_CHOICES = [
        ('2019/2020', '2019/2020'),
        ('2020/2021', '2020/2021'),
        ('2021/2022', '2021/2022'),
        ('2022/2023', '2022/2023')
    ]
    mata_kuliah = models.CharField(max_length =50)
    dosen = models.CharField(max_length =20)
    sks = models.IntegerField()
    semester = models.CharField(
        max_length=6, 
        choices= TERM_CHOICES,
        default= 'Ganjil')
    tahun = models.CharField(
        max_length=9,
        choices= YEAR_CHOICES,
        default='2019/2020'
    )
    ruang = models.CharField(max_length =20)
    desc = models.TextField()

    published = models.DateTimeField(auto_now_add = True)
    updated = models.DateTimeField(auto_now = True)

    def __str__ (self):
        return self.mata_kuliah
        
        