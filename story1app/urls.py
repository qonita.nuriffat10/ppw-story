from django.urls import path

from . import views

app_name = 'story1app'

urlpatterns = [
    path('story1/', views.profile, name='index'),
]